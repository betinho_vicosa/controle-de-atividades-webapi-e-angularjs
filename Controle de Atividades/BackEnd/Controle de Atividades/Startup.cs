﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Swashbuckle.Application;
using System.Linq;

[assembly: OwinStartup(typeof(Controle_de_Atividades.Startup))]

namespace Controle_de_Atividades
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            var formatters = config.Formatters;
            formatters.Remove(formatters.XmlFormatter);

            var jsonSettings = formatters.JsonFormatter.SerializerSettings;
            jsonSettings.Formatting = Formatting.Indented;
            jsonSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            formatters.JsonFormatter.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.Objects;
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultRoute",
                routeTemplate: "api/{controller}/{id}",

                defaults: new { id = RouteParameter.Optional }
            );

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            config.EnableSwagger(c =>
            {
                c.SingleApiVersion("Help", "WebAPI");
                c.IncludeXmlComments(GetXml());
                 c.ResolveConflictingActions(x => x.First());

            }).EnableSwaggerUi();

            app.UseWebApi(config);
        }

        private static string GetXml()
        {
            return string.Format(@"{0}\bin\Controle de Atividades.XML",
                    AppDomain.CurrentDomain.BaseDirectory);
        }
    }
}
