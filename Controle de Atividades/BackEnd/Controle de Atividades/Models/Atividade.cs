﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Controle_de_Atividades.Models
{
    public enum StatusAtividade
    {
        Aguardando = 1,
        EmAndamento = 2,
        Concluida = 3,
        Cancelada = 4
    }

    public class Atividade
    {
        [Key]
        public int AtividadeID { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public DateTime Data { get; set; }
        public StatusAtividade Status { get; set; }
    }
}