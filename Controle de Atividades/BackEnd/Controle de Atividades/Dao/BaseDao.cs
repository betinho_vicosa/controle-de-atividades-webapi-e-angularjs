﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Controle_de_Atividades.Dao
{
    public sealed class BaseDao<TEntity> : IDisposable where TEntity : class
    {
        DataBaseContext ctx = DataBaseContext.Create();
        public async Task<IQueryable<TEntity>> GetAll()
        {
            var lista = await ctx.Set<TEntity>().ToListAsync();
            return lista.AsQueryable();
        }

        public async Task<IQueryable<TEntity>> Get(Func<TEntity, bool> predicate)
        {
            var lista = await ctx.Set<TEntity>().ToListAsync();
            return lista.Where(predicate).AsQueryable();
        }

        public async Task<TEntity> Find(params object[] key)
        {
            return await ctx.Set<TEntity>().FindAsync(key);
        }

        public void Atualizar(TEntity obj)
        {
            ctx.Entry(obj).State = EntityState.Modified;
        }

        public async Task SalvarTodos()
        {
            await ctx.SaveChangesAsync();
        }

        public TEntity Adicionar(TEntity obj)
        {
            return ctx.Set<TEntity>().Add(obj);
        }

        public void Excluir(Func<TEntity, bool> predicate)
        {
            ctx.Set<TEntity>()
                .Where(predicate).ToList()
                .ForEach(del => ctx.Set<TEntity>().Remove(del));
        }

        public void Dispose()
        {
            ctx.Dispose();
        }
    }
}