﻿using System;

namespace Controle_de_Atividades.Dao
{
    public static class DaoFactory
    {
        public static BaseDao<Tmodelo> Create<Tmodelo>() where Tmodelo : class
        {
            return Activator.CreateInstance<BaseDao<Tmodelo>>();
        }
    }
}