namespace Controle_de_Atividades.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Controle_de_Atividades.DataBaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Controle_de_Atividades.DataBaseContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            context.Atividades.AddOrUpdate(
              new Models.Atividade { Data = DateTime.Now, Titulo = "Lavar", Descricao = "Lavar roupa", Status = Models.StatusAtividade.Aguardando},
              new Models.Atividade { Data = DateTime.Now, Titulo = "Passar", Descricao = "Passar roupa", Status = Models.StatusAtividade.Cancelada },
              new Models.Atividade { Data = DateTime.Now, Titulo = "Cozinhar", Descricao = "Lavar roupa", Status = Models.StatusAtividade.EmAndamento },
              new Models.Atividade { Data = DateTime.Now, Titulo = "Dar banho nas crian�as", Descricao = "Lavar roupa", Status = Models.StatusAtividade.Concluida}
              
            );
            //
        }
    }
}
