﻿using Controle_de_Atividades.BindModels;
using Controle_de_Atividades.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Controle_de_Atividades.BindModels
{
    public abstract class AtividadesBindModels
    {
        public class AtividadeCreateModel
        {
            [Required]
            [Display(Name = "Data da Atividade")]
            public DateTime Data { get; set; }

            [Required]
            [Display(Name = "Descrição da Atividade")]
            public string Descricao { get; set; }

            [Required]
            [Display(Name = "Status Inicial da Atividade")]
            public StatusAtividade Status { get; set; }

            [Required]
            [Display(Name = "Titulo da Atividade")]
            public string Titulo { get; set; }
        }

        public class AtividadeUpdateModel
        {
            [Required]
            [Display(Name = "Id da Atividade")]
            public int AtividadeID { get; set; }

            [Display(Name = "Data da Atividade")]
            public DateTime Data { get; set; }

            [Display(Name = "Descrição da Atividade")]
            public string Descricao { get; set; }

            [Display(Name = "Status Inicial da Atividade")]
            public StatusAtividade Status { get; set; }

            [Display(Name = "Titulo da Atividade")]
            public string Titulo { get; set; }
        }
    }
}