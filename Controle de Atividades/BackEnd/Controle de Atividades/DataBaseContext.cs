﻿using Controle_de_Atividades.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Controle_de_Atividades
{
    public class DataBaseContext : DbContext
    {
        public DbSet<Atividade> Atividades { get; set; }


        public DataBaseContext()
            : base("DefaultConnection")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public static DataBaseContext Create()
        {
            return new DataBaseContext();
        }

    }
}