﻿using Controle_de_Atividades.Dao;
using Controle_de_Atividades.Models;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using static Controle_de_Atividades.BindModels.AtividadesBindModels;

namespace Controle_de_Atividades.Controllers
{
    [RoutePrefix("api/atividades")]
    public class AtividadesController : ApiController
    {
        /// <summary>
        /// Retorna todas as atividades
        /// </summary>
        /// <returns>
        /// 200 - Sucesso,
        /// 400 - Erro 
        /// </returns>
        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> GetTodos()
        {
            try
            {
                using (var dao = DaoFactory.Create<Atividade>())
                {
                    var lista = await dao.GetAll();
                    if (lista.Any())
                        return Ok(lista);
                    else
                        return BadRequest("Nenhum Registro encontrado");
                }

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        /// <summary>
        /// Retorna atividade por Id
        /// </summary>
        /// <returns>
        /// 200 - Sucesso,
        /// 400 - Erro 
        /// </returns>
        [HttpGet]
        [Route("{id:int}", Name = "GetAtividadePorId")]
        public async Task<IHttpActionResult> GetAtividadePorId(int id)
        {
            try
            {
                using (var dao = DaoFactory.Create<Atividade>())
                {
                    var atividade = await dao.Find(id);
                    if (atividade != null)
                        return Ok(atividade);
                    else
                        return BadRequest("Nenhum Registro encontrado");
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Retorna a atividade por titulo
        /// </summary>
        /// <param name="titulo">Ex: Lavar roupas</param>
        /// <returns>
        /// 200 - Sucesso,
        /// 400 - Erro 
        /// </returns>
        [HttpGet]
        [Route("titulo/{titulo}")]
        public async Task<IHttpActionResult> GetAtividadePorTitulo(string titulo)
        {
            try
            {
                using (var dao = DaoFactory.Create<Atividade>())
                {
                    var atividade = await dao.Get(a => a.Titulo.Equals(titulo));
                    if (atividade.Any())
                        return Ok(atividade.FirstOrDefault());
                    else
                        return BadRequest("Nenhum Registro encontrado");
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Busca de atividades por data de inclusão
        /// </summary>
        /// <param name="data">Ex: 02-11-2017</param>
        /// <returns>
        /// 200 - Sucesso,
        /// 400 - Erro 
        /// </returns>
        [HttpGet]
        //[Route("data/{data:DateTime}")]
       [Route("data")]
        public async Task<IHttpActionResult> GetAtividadePorData(DateTime data)
        {
            try
            {
                using (var dao = DaoFactory.Create<Atividade>())
                {
                    var atividade = await dao.Get(a => a.Data.ToString("dd/MM/yyyy").Equals(data.ToString("dd/MM/yyyy")));
                    if (atividade.Any())
                        return Ok(atividade);
                    else
                        return BadRequest("Nenhum Registro encontrado");
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Cria nova Atividade
        /// </summary>
        /// <returns>
        /// 201 - Sucesso,
        /// 400 - Erro 
        /// </returns>
        [HttpPost]
        [Route("cadastrar")]
        public async Task<IHttpActionResult> PostCadastro(AtividadeCreateModel model)
        {
            try
            {
                if (ModelState.IsValid)
                    using (var dao = DaoFactory.Create<Atividade>())
                    {
                        var atividade = new Atividade
                        {
                            Data = model.Data,
                            Titulo = model.Titulo,
                            Descricao = model.Descricao,
                            Status = model.Status
                        };

                        var novo = dao.Adicionar(atividade);
                        await dao.SalvarTodos();
                        Uri location = new Uri(Url.Link("GetAtividadePorId", new { id = novo.AtividadeID }));
                        return Created(location, atividade);
                    }
                else
                    throw new HttpRequestValidationException("Atividade Fora do Padrão Esperado");

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// Atualiza Atividade
        /// </summary>
        /// <returns>
        /// 200 - Sucesso,
        /// 400 - Erro 
        /// </returns>
        [HttpPut]
        [Route("atualizar")]
        public async Task<IHttpActionResult> PutAtividade(AtividadeUpdateModel model)
        {
            try
            {
                if (ModelState.IsValid)
                    using (var dao = DaoFactory.Create<Atividade>())
                    {
                        var atividade = new Atividade
                        {
                            AtividadeID = model.AtividadeID,
                            Data = model.Data,
                            Titulo = model.Titulo,
                            Descricao = model.Descricao,
                            Status = model.Status
                        };

                        dao.Atualizar(atividade);
                        await dao.SalvarTodos();
                        return Ok(true);
                    }
                else
                    throw new HttpRequestValidationException("Atividade Fora do Padrão Esperado");

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// Remove Atividade
        /// </summary>
        /// <returns>
        /// 202 - Sucesso,
        /// 400 - Erro 
        /// </returns>
        [HttpDelete]
        [Route("{id:int}")]
        public async Task<IHttpActionResult> DeleteAtividadePorId(int id)
        {
            try
            {
                using (var dao = DaoFactory.Create<Atividade>())
                {
                    dao.Excluir(a => a.AtividadeID.Equals(id));
                    await dao.SalvarTodos();
                    return Ok(true);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }

}

