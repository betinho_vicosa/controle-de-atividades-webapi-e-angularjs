(function () {
    'use strict';

    angular.module('App', ['ui.router', 'angular-loading-bar','ui.mask','ngDraggable'])
        .config(function ($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/home');
            $stateProvider

                .state('home', {
                    url: '/home',
                    templateUrl: 'app/views/home.html',
                    controller: "HomeController",
                    controllerAs: "homeController"
                });
        });
})();