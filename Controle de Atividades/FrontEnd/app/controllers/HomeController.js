(function () {
    'use strict';

    angular
        .module('App')
        .controller('HomeController', HomeController);

    HomeController.inject = ['Services', 'maskService', '$filter'];

    function HomeController(Services, maskService, $filter) {
        var vm = this;
        vm.mask = maskService;
        vm.atividade = {};
        vm.listaAtividades = {};
        vm.descricaoStatus = function (cod) {
            switch (cod) {
                case 1:
                    return "Aguardando";
                case 2:
                    return "Em Andamento";
                case 3:
                    return "Concluida";
                case 4:
                    return "Cancelada";
            }
        };

        vm.listaStatus = [{
                "codigo": 1,
                "descricao": "Aguardando"
            },
            {
                "codigo": 2,
                "descricao": "Em Andamento"
            },
            {
                "codigo": 3,
                "descricao": "Concluida"
            },
            {
                "codigo": 4,
                "descricao": "Cancelada"
            }
        ];


        vm.cadastrar = function () {
            Services.PostCadastro(vm.atividade)
                .then(function (atividade) {
                    console.log(atividade);
                    vm.atividade = {};
                    vm.GetTodos();
                })
                .catch(function (erro) {
                    console.log(erro);
                });
        };

        vm.atualizar = function () {
            Services.PutAtividade(vm.atividade)
                .then(function (atividade) {
                    console.log(atividade);
                    vm.atividade = {};
                    vm.GetTodos();
                })
                .catch(function (erro) {
                    console.log(erro);
                });
        };

        vm.salvar = function () {
            if (typeof vm.atividade.atividadeID === 'undefined' || vm.atividade.atividadeID === null) {
                vm.cadastrar();
            } else {
                vm.atualizar();
            }
        };

        vm.modificarRegistro = function (atividade) {
            vm.atividade = atividade;
            vm.atividade.data = $filter('date')(atividade.data, 'dd/MM/yyyy');
        };

        vm.deletar = function (atividadeID) {
            Services.DeleteAtividadePorId(atividadeID)
                .then(function (sucesso) {
                    console.log(sucesso);
                    vm.GetTodos();
                })
                .catch(function (erro) {
                    console.log(erro);
                });
        };

        vm.GetTodos = function () {
            Services.GetTodos()
                .then(function (atividades) {
                    vm.listaAtividades = atividades;
                })
                .catch(function (erro) {
                    vm.listaAtividades = {};
                    console.log(erro);
                });
        };
        vm.GetPorData = function () {
            Services.GetAtividadePorData(vm.dataPesquisa)
                .then(function (atividades) {
                    vm.listaAtividades = atividades;
                })
                .catch(function (erro) {
                    vm.listaAtividades = {};
                    console.log(erro);
                });
        };

        inicializar();

        function inicializar() {
            vm.GetTodos();
        }
    }
})();