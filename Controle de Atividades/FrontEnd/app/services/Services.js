(function () {
    'use strict';

    angular
        .module('App')
        .factory('Services', Services);

    Services.inject = ['$http'];

    function Services($http) {
        var service = {};
        var servidor = 'http://stopnutsteste.azurewebsites.net/api/';
        service.GetTodos = function () {
            return $http.get(servidor + 'atividades')
                .then(function (resposta) {
                    return resposta.data;
                });
        };

        service.GetAtividadePorData = function (data) {
            return $http.get(servidor + 'atividades/data?data=' + data)
                .then(function (resposta) {
                    return resposta.data;
                });
        };

        service.PostCadastro = function (atividade) {
            return $http.post(servidor + 'atividades/cadastrar', atividade)
                .then(function (resposta) {
                    return resposta.data;
                });
        };
        service.PutAtividade = function (atividade) {
            return $http.put(servidor + 'atividades/atualizar', atividade)
                .then(function (resposta) {
                    return resposta.data;
                });
        };
        service.DeleteAtividadePorId = function (atividadeID) {
            return $http.delete(servidor + 'atividades/' + atividadeID)
                .then(function (resposta) {
                    return resposta.data;
                });
        };

        return service;

    }
})();