(function() {
'use strict';

    angular
        .module('App')
        .service('maskService', maskService);

    maskService.$inject = [];
    function maskService() {
        this.data = "99/99/9999";
    }
})();